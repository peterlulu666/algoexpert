# compare add up with target
# if add up small move left pointer
# else move right pointer
def TwoNumberSum(nums:list, target:int):
    left_ptr = 0
    right_ptr = len(nums) - 1
    nums.sort()
    while left_ptr < right_ptr:
        if nums[left_ptr] + nums[right_ptr] == target:
            return [nums[left_ptr], nums[right_ptr]]
        elif nums[left_ptr] + nums[right_ptr] < target:
            left_ptr = left_ptr + 1
        else:
            right_ptr = right_ptr - 1


print(TwoNumberSum([1, 3, 4, 5], 7))
print(TwoNumberSum([3, 5, -4, 8, 11, 1, -1, 6], 10))
print(TwoNumberSum([11, 5, -4, 8, 3, 1, 6, -1], 10))
