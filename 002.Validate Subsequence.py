# if not same move main pointer
# if same append and move main pointer and sub pointer
# compare res with sub_list
def ValidateSubsequence(main_list: list, sub_list: list):
    if len(main_list) < len(sub_list):
        return False
    main_ptr = 0
    sub_ptr = 0
    res = []
    while main_ptr < len(main_list) and sub_ptr < len(sub_list):
        if main_list[main_ptr] != sub_list[sub_ptr]:
            main_ptr = main_ptr + 1
        else:
            res.append(main_list[main_ptr])
            main_ptr = main_ptr + 1
            sub_ptr = sub_ptr + 1
    return res == sub_list


print(ValidateSubsequence([5, 1, 22, 25, 6, -1, 8, 10], [1, 6, -1, 10]))
print(ValidateSubsequence([3, 1, 7, 5, 10, 2], [1, 5, 2]))
print(ValidateSubsequence([3, 1, 7, 5, 10, 2], [1, 5, 3]))
