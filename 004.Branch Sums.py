class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def BranchSums(root: TreeNode):
    if root is None:
        return []

    def preorder(root: TreeNode, prev_add_up: int, add_up_list: list):
        if root is None:
            return []
        value = root.val
        current_add_up = value + prev_add_up
        if root.left is None and root.right is None:
            add_up_list.append(current_add_up)
        preorder(root.left, current_add_up, add_up_list)
        preorder(root.right, current_add_up, add_up_list)

    res = []
    preorder(root, 0, res)
    return res


#           1
#          /  \
#         2    3
#        / \   / \
#       4   5  6 7
#      / \
#     8  9

root1 = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)
node6 = TreeNode(6)
node7 = TreeNode(7)
node8 = TreeNode(8)
node9 = TreeNode(9)

root1.left = node2
root1.right = node3
node2.left = node4
node2.right = node5
node4.left = node8
node4.right = node9
node3.left = node6
node3.right = node7

print(BranchSums(root1))
