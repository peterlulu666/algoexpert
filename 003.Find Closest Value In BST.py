class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# recursive
# def FindClosestValueInBST(root: TreeNode, target: int):
#     def search(root: TreeNode, target: int, closest=None, difference=float("inf")):
#         if root is None:
#             # print("closest", closest)
#             return closest
#         if abs(target - root.val) < difference:
#             difference = abs(target - root.val)
#             closest = root.val
#         if target == root.val:
#             # print("closest", target)
#             return target
#         elif target < root.val:
#             return search(root.left, target, closest, difference)
#         elif target > root.val:
#             return search(root.right, target, closest, difference)
#
#     return search(root, target)

# iterative
def FindClosestValueInBST(root: TreeNode, target: int):
    if root is None:
        return
    difference = float("inf")
    closest = None
    while root:
        if abs(target - root.val) < difference:
            difference = abs(target - root.val)
            closest = root.val
        if target == root.val:
            return target
        elif target < root.val:
            root = root.left
        else:
            root = root.right
    return closest


#           10
#          /  \
#         5    15
#        / \   / \
#       2   6  13 22
#      /        \
#     1          14
root10 = TreeNode(10)
node5 = TreeNode(5)
node2 = TreeNode(2)
node1 = TreeNode(1)
node6 = TreeNode(6)
node15 = TreeNode(15)
node13 = TreeNode(13)
node22 = TreeNode(22)
node14 = TreeNode(14)

root10.left = node5
root10.right = node15
node5.left = node2
node5.right = node6
node2.left = node1
node15.left = node13
node15.right = node22
node13.right = node14

print(FindClosestValueInBST(root10, 16))
