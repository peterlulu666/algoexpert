from collections import deque


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def NodeDepths(root: TreeNode):
    if root is None:
        return -1

    q = deque()
    q.append((root, 0))
    level_list = []

    while len(q) > 0:
        node = q.popleft()
        level = node[1]
        level_list.append(level)
        if node[0].left is not None:
            q.append((node[0].left, level + 1))
        if node[0].right is not None:
            q.append((node[0].right, level + 1))

    return sum(level_list)


#           1
#          /  \
#         2    3
#        / \
#       4   5


root1 = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)

root1.left = node2
root1.right = node3
node2.left = node4
node2.right = node5

print(NodeDepths(root1))
