# Calculate list squared
# Use two pointer to sort the squared list
def SortedSquaredArray(nums: list):
    squared_list = [num ** 2 for num in nums]
    left_pointer = 0
    right_pointer = len(nums) - 1
    res = []
    while left_pointer <= right_pointer:
        if squared_list[left_pointer] < squared_list[right_pointer]:
            res.append(squared_list[right_pointer])
            right_pointer = right_pointer - 1
        else:
            res.append(squared_list[left_pointer])
            left_pointer = left_pointer + 1
    res.reverse()
    return res


print(SortedSquaredArray([-4, -2, 0, 1, 3]))
print(SortedSquaredArray([1, 2, 3, 4, 5]))
print(SortedSquaredArray([-5, -4, -3, -2, -1]))
